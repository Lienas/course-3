package com.udacity.course3.repository;

import com.udacity.course3.data.Delivery;
import com.udacity.course3.data.Plant;
import com.udacity.course3.data.RecipientAndPrice;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional

public class DeliveryRepository {

    @PersistenceContext
    EntityManager entityManager;

    private static final String GET_RECIPIENT_AND_PRICE =
            "select new com.udacity.course3.data.RecipientAndPrice.RecipientAndPrice(d.name, d.plants.price)" +
                    "from delivery d" +
                    "where d.id = :id";


    public void persist(Delivery delivery) {
        entityManager.persist(delivery);
    }

    public Delivery find(Long id) {
        return entityManager.find(Delivery.class, id);
    }

    public Delivery merge(Delivery delivery) {
        return entityManager.merge(delivery);
    }

    public void delete(Long id) {
        Delivery delivery = entityManager.find(Delivery.class, id);
        entityManager.remove(delivery);
    }

    public List<Delivery> findDeliveriesByName(String name) {
        TypedQuery<Delivery> query =
                entityManager.createNamedQuery("Delivery.findByName", Delivery.class);
        query.setParameter("name", name);
        return query.getResultList();
    }

    public RecipientAndPrice getBill(Long deliveryId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<RecipientAndPrice> query =
                cb.createQuery(RecipientAndPrice.class);

        Root<Plant> root = query.from(Plant.class);

        query.select(
                cb.construct(
                        RecipientAndPrice.class,
                        root.get("delivery").get("name"),
                        cb.sum(root.get("price"))))
                .where(cb.equal(root.get("delivery").get("id"), deliveryId));

        return entityManager.createQuery(query).getSingleResult();
    }

}
