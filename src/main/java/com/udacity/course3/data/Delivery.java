package com.udacity.course3.data;

import org.hibernate.annotations.Nationalized;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity

@NamedQuery(
        name = "Delivery.findByName",
        query = "select d from Delivery d where d.name = :name"
)

public class Delivery {

    @Id
    @GeneratedValue
    private Long id;

    private LocalDateTime deliveryTime;

    @Nationalized
    private String name;

    @Column(length = 500, name = "address_full")
    private String address;

    @Type(type = "yes_no")
    private Boolean completed;

    @OneToMany(mappedBy = "delivery", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Plant> plants;

    //<editor-fold desc="getters and setters">
    public String getName() {
        return name;
    }

    public void setName(String recipient) {
        this.name = recipient;
    }


    public LocalDateTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalDateTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public List<Plant> getPlants() {
        return plants;
    }

    public void setPlants(List<Plant> deliveries) {
        this.plants = deliveries;
    }
    //</editor-fold>

}
